(function () {
    'use strict';
    module.exports = {
        devServer: {
            contentBase: "./",
            hot: true,
            port: 1327,
            historyApiFallback: true
        }
    };
})();