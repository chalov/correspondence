(function (angular) {


    var demoData = [];


    for (var i = 0; i < 15; i++) {
        demoData.push({
            id: (i + 1),
            subject: 'Письмо ' + (i + 1),
            created: '2016-03-01 23:59',
            parts: [
                {
                    id: 1,
                    author: 'vasya',
                    text: 'Привет, как дела?'
                },
                {
                    id: 2,
                    author: 'petya',
                    text: 'Привет, все хорошо, спасибо!'
                }
            ]
        })
    }

    angular.module('correspondence')
        .factory('fetchDialogs', ['$q',
            function ($q) {
                return function () {
                    var deferred = $q.defer();

                    setTimeout(function () {
                        deferred.resolve(demoData)
                    }, 1500);

                    return deferred.promise;
                }
            }
        ])
        .factory('fetchDialog', ['$q',
            function ($q) {
                return function (dialog_id) {
                    var deferred = $q.defer(),
                        dialog = null;
                    for (var i = 0, ii = demoData.length; i < ii; i++) if (dialog_id === demoData[i].id) dialog = demoData[i];


                    setTimeout(function () {
                        deferred.resolve(dialog)
                    }, 3000);


                    return deferred.promise;
                }
            }
        ]);
})(window.angular);