(function (angular) {

    angular.module('correspondence')
        .config([
            '$stateProvider', '$urlRouterProvider', '$locationProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider) {

                $stateProvider
                    .state('main', {
                        abstract: true,
                        templateUrl: "../partials/main.html"
                    })
                    .state('main.dialogs', {
                        url: "",
                        views: {
                            "main.dialogs": {
                                templateUrl: "../partials/dialogs.html",
                                controllerAs: 'dialogs',
                                controller: 'dialogs'
                            }
                        }
                    })
                    .state('main.dialogs.dialog', {
                        url: "/:dialogId",
                        views: {
                            "main.dialog": {
                                templateUrl: "../partials/dialog.html",
                                controllerAs: 'dialog',
                                controller: 'dialog'
                            }
                        }
                    });

                $locationProvider.html5Mode(true).hashPrefix('!');
                $urlRouterProvider.otherwise("");
            }
        ]);
})(window.angular);