(function (angular) {

    angular.module('correspondence')
        .controller('dialogs', ['$scope', 'fetchDialogs', '$state',
            function (scope, fetchDialogs, $state) {
                var $scope = this;

                scope.$state = $state;

                $scope.loading = true;

                $scope.dialogs = [];

                fetchDialogs().then(function (dialogs) {
                    $scope.loading = false;

                    $scope.dialogs = dialogs;
                });


            }
        ]);
})(window.angular);