(function (angular) {

    angular.module('correspondence')
        .controller('dialog', ['$scope', 'fetchDialog', '$stateParams', '$timeout',
            function (scope, fetchDialog, $stateParams, $timeout) {
                var $scope = this,
                    message_num = 0,
                    dialog_id = parseInt($stateParams.dialogId);

                isNaN(dialog_id) && (dialog_id = null);

                $scope.sending = false;

                $scope.message = '';

                $scope.dialog = null;
                $scope.loading = dialog_id !== null;

                dialog_id !== null && fetchDialog(dialog_id)
                    .then(function (dialog) {
                        $scope.dialog = dialog;
                        $scope.loading = false;
                    });

                $scope.send_message = function () {
                    $scope.sending = true;

                    $timeout(function () {
                        $scope.dialog.parts.push({
                            id: 5,
                            author: 'petr',
                            text: $scope.message
                        });

                        $scope.message = '';

                        $scope.sending = false;
                    }, 1000);
                };
            }
        ]);
})(window.angular);